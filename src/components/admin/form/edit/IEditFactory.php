<?php

declare(strict_types=1);

namespace Skadmin\TwitchStreamer\Components\Admin;

interface IEditFactory
{
    public function create(?int $id = null): Edit;
}
