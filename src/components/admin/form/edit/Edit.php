<?php

declare(strict_types=1);

namespace Skadmin\TwitchStreamer\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use Skadmin\TwitchStreamer\BaseControl;
use Skadmin\TwitchStreamer\Doctrine\TwitchStreamer\TwitchStreamer;
use Skadmin\TwitchStreamer\Doctrine\TwitchStreamer\TwitchStreamerFacade;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory        $webLoader;
    private TwitchStreamerFacade $facade;
    private TwitchStreamer       $twitchStreamer;
    private ImageStorage         $imageStorage;

    public function __construct(?int $id, TwitchStreamerFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->twitchStreamer = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->twitchStreamer->isLoaded()) {
            return new SimpleTranslation('twitch-streamer.edit.title - %s', $this->twitchStreamer->getName());
        }

        return 'twitch-streamer.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($this->twitchStreamer->isLoaded()) {
            if ($identifier !== null && $this->twitchStreamer->getImagePreview() !== null) {
                $this->imageStorage->delete($this->twitchStreamer->getImagePreview());
            }

            $twitchStreamer = $this->facade->update(
                $this->twitchStreamer->getId(),
                $values->name,
                $values->website,
                $values->is_active,
                $values->code,
                $identifier
            );
            $this->onFlashmessage('form.twitch-streamer.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $twitchStreamer = $this->facade->create(
                $values->name,
                $values->website,
                $values->is_active,
                $values->code,
                $identifier
            );
            $this->onFlashmessage('form.twitch-streamer.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $twitchStreamer->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->twitchStreamer = $this->twitchStreamer;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.twitch-streamer.edit.name')
            ->setRequired('form.twitch-streamer.edit.name.req');
        $form->addText('code', 'form.twitch-streamer.edit.code');
        $form->addText('website', 'form.twitch-streamer.edit.website');
        $form->addCheckbox('is_active', 'form.twitch-streamer.edit.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('image_preview', 'form.twitch-streamer.edit.image-preview');

        // BUTTON
        $form->addSubmit('send', 'form.twitch-streamer.edit.send');
        $form->addSubmit('send_back', 'form.twitch-streamer.edit.send-back');
        $form->addSubmit('back', 'form.twitch-streamer.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->twitchStreamer->isLoaded()) {
            return [];
        }

        return [
            'name'      => $this->twitchStreamer->getName(),
            'code'      => $this->twitchStreamer->getCode(),
            'is_active' => $this->twitchStreamer->isActive(),
            'website'   => $this->twitchStreamer->getWebsite(),
        ];
    }
}
