<?php

declare(strict_types=1);

namespace Skadmin\TwitchStreamer\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
