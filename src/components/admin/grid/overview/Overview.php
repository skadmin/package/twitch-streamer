<?php

declare(strict_types=1);

namespace Skadmin\TwitchStreamer\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use Skadmin\TwitchStreamer\BaseControl;
use Skadmin\TwitchStreamer\Doctrine\TwitchStreamer\TwitchStreamer;
use Skadmin\TwitchStreamer\Doctrine\TwitchStreamer\TwitchStreamerFacade;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function sprintf;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private TwitchStreamerFacade $facade;
    private LoaderFactory        $webLoader;
    private ImageStorage         $imageStorage;

    public function __construct(TwitchStreamerFacade $facade, Translator $translator, ImageStorage $imageStorage, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
        $this->webLoader    = $webLoader;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'twitch-streamer.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (TwitchStreamer $twitchStreamer): ?Html {
                if ($twitchStreamer->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$twitchStreamer->getImagePreview(), '60x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.twitch-streamer.overview.name')
            ->setRenderer(function (TwitchStreamer $twitchStreamer): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $twitchStreamer->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $render = new Html();
                $name->setText($twitchStreamer->getName());

                $render->addHtml($name);
                if ($twitchStreamer->getCode() !== '') {
                    $code = Html::el('code', ['class' => 'text-muted small'])->setText($twitchStreamer->getCode());

                    $render->addHtml('<br/>')
                        ->addHtml($code);
                }

                return $render;
            });
        $grid->addColumnText('website', 'grid.twitch-streamer.overview.website')
            ->setRenderer(static function (TwitchStreamer $twitchStreamer): ?Html {
                if ($twitchStreamer->getWebsite() === '') {
                    return null;
                }

                $icon = Html::el('small', ['class' => 'fas fa-external-link-alt mr-1']);

                return Html::el('a', [
                    'href'   => $twitchStreamer->getWebsite(),
                    'target' => '_blank',
                ])->setHtml($icon)
                    ->addText($twitchStreamer->getWebsite());
            });
        $this->addColumnIsActive($grid, 'twitch-streamer.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.twitch-streamer.overview.name', ['name', 'code']);
        $this->addFilterIsActive($grid, 'twitch-streamer.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.twitch-streamer.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.twitch-streamer.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.twitch-streamer.overview.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
