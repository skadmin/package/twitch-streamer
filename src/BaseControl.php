<?php

declare(strict_types=1);

namespace Skadmin\TwitchStreamer;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'twitch-streamer';
    public const DIR_IMAGE = 'twitch-streamer';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fab fa-fw fa-twitch']),
            'items'   => ['overview'],
        ]);
    }
}
