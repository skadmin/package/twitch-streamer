<?php

declare(strict_types=1);

namespace Skadmin\TwitchStreamer\Doctrine\TwitchStreamer;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

use function str_replace;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TwitchStreamer
{
    use Entity\Id;
    use Entity\Name;
    use Entity\IsActive;
    use Entity\Website;
    use Entity\Code;
    use Entity\ImagePreview;
    use Entity\Sequence;

    private bool   $isLive        = false;
    private string $streamPreview = '';

    public function update(string $name, bool $isActive, string $website, string $code, ?string $imagePreview): void
    {
        $this->name     = $name;
        $this->isActive = $isActive;
        $this->website  = $website;

        $this->setCode($code);

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function setStreamData(bool $isLive, string $streamPreview): void
    {
        $this->isLive        = $isLive;
        $this->streamPreview = $streamPreview;
    }

    public function isLive(): bool
    {
        return $this->isLive;
    }

    public function getStreamPreview(int $width = 320, int $height = 180): string
    {
        $template = $this->streamPreview;
        $template = str_replace('{width}', (string) $width, $template);
        $template = str_replace('{height}', (string) $height, $template);

        return $template;
    }
}
