<?php

declare(strict_types=1);

namespace Skadmin\TwitchStreamer\Doctrine\TwitchStreamer;

use Doctrine\ORM\QueryBuilder;
use Exception;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Setting\Doctrine\Setting\Setting;
use Skadmin\Setting\Doctrine\Setting\SettingFacade;
use SkadminUtils\DoctrineTraits\Facade;

use function array_slice;
use function array_values;

final class TwitchStreamerFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    private TwitchStreamerService $service;
    private ?Setting              $twitchClientId;

    public function __construct(EntityManagerDecorator $em, SettingFacade $facadeSetting)
    {
        parent::__construct($em);
        $this->twitchClientId = $facadeSetting->findByCode('apiTwitchCliendId'); // @phpstan-ignore-line

        if ($this->twitchClientId === null) {
            throw new Exception('Twich client id [apiTwitchCliendId] not found');
        }

        $this->table   = TwitchStreamer::class;
        $this->service = new TwitchStreamerService($this->twitchClientId->getValue());
    }

    public function create(string $name, string $website, bool $isActive, string $code, ?string $imagePreview): TwitchStreamer
    {
        return $this->update(null, $name, $website, $isActive, $code, $imagePreview);
    }

    public function update(?int $id, string $name, string $website, bool $isActive, string $code, ?string $imagePreview): TwitchStreamer
    {
        $twitchStreamer = $this->get($id);
        $twitchStreamer->update($name, $isActive, $website, $code, $imagePreview);

        if (! $twitchStreamer->isLoaded()) {
            $twitchStreamer->setSequence($this->getValidSequence());
        }

        $this->em->persist($twitchStreamer);
        $this->em->flush();

        return $twitchStreamer;
    }

    public function get(?int $id = null): TwitchStreamer
    {
        if ($id === null) {
            return new TwitchStreamer();
        }

        $twitchStreamer = parent::get($id);

        if ($twitchStreamer === null) {
            return new TwitchStreamer();
        }

        return $twitchStreamer;
    }

    /**
     * @return TwitchStreamer[]
     */
    public function getAll(bool $onlyActive = false, bool $onlyLive = false): array
    {
        return $this->getByLimit(null, $onlyActive, $onlyLive);
    }

    /**
     * @return TwitchStreamer[]
     */
    public function getByLimit(?int $limit = null, bool $onlyActive = false, bool $onlyLive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        $streamers = $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);

        $result = array_values($this->service->loadStreamerData($streamers, $onlyLive));

        if ($limit === null) {
            return $result;
        }

        return array_slice($result, 0, $limit);
    }

    public function getModel(?string $table = null): QueryBuilder
    {
        return parent::getModel($table)
            ->orderBy('a.sequence');
    }
}
