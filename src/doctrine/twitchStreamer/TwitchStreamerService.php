<?php

declare(strict_types=1);

namespace Skadmin\TwitchStreamer\Doctrine\TwitchStreamer;

use Nette\Utils\Strings;
use TwitchApi\TwitchApi;

use function implode;
use function is_array;
use function json_decode;

final class TwitchStreamerService
{
    private string $twitchClientId;

    public function __construct(string $twitchClientId)
    {
        $this->twitchClientId = $twitchClientId;
    }

    /**
     * @param TwitchStreamer[] $streamers
     *
     * @return TwitchStreamer[]
     */
    public function loadStreamerData(array $streamers, bool $onlyLive = false): array
    {
        /** @var TwitchStreamer[] $streamersResult */
        $streamersResult = [];

        /** @var TwitchStreamer[] $streamersResultOffline */
        $streamersResultOffline = [];

        $clientsId = [];
        foreach ($streamers as $streamer) {
            if ($streamer->getCode() === '') {
                continue;
            }

            $clientsId[]                                           = $streamer->getCode();
            $streamersResult[Strings::lower($streamer->getCode())] = $streamer;
        }

        $twitchApi = $this->getApiTwich();
        $twitchApi->setTimeout(10);
        $twitchStreamers = $twitchApi->getUserByUsername(implode(',', $clientsId));

        if (! is_array($twitchStreamers)) {
            $twitchStreamers = json_decode($twitchStreamers, true);
        }

        if (isset($twitchStreamers['users'])) {
            foreach ($twitchStreamers['users'] as $twitchStreamer) {
                $stream = $twitchApi->getStreamByUser($twitchStreamer['_id']);

                if (! is_array($stream)) {
                    $stream = json_decode($stream, true);
                }

                if ($stream['stream'] === null && $onlyLive) {
                    unset($streamersResult[$twitchStreamer['name']]);
                } elseif ($stream['stream'] === null) {
                    $streamersResultOffline[$twitchStreamer['name']] = $streamersResult[$twitchStreamer['name']];
                    unset($streamersResult[$twitchStreamer['name']]);
                } else {
                    $streamersResult[$twitchStreamer['name']]->setStreamData(true, $stream['stream']['preview']['template']);
                }
            }
        }

        return $streamersResult + $streamersResultOffline;
    }

    private function getApiTwich(): TwitchApi
    {
        $options = ['client_id' => $this->twitchClientId];

        return new TwitchApi($options);
    }
}
