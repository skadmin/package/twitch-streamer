<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200405105524 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $sections = [
            [
                'id'          => 4,
                'name'        => 'setting-section.general-settings.name',
                'webalize'    => 'general-settings',
                'description' => 'setting-section.general-settings.description',
            ],
        ];

        foreach ($sections as $section) {
            $this->addSql('INSERT INTO core_setting_section (id, webalize, name, description) VALUES (:id, :webalize, :name, :description)', $section);
        }

        $settings = [
            [
                'section_id'  => 4,
                'name'        => 'Api twitch client ID',
                'code'        => 'apiTwitchCliendId',
                'value'       => '',
                'description' => 'setting-setting.api-twitch-cliend-id.description',
            ],
        ];

        foreach ($settings as $setting) {
            $this->addSql('INSERT INTO core_setting (section_id, name, code, value, description) VALUES (:section_id, :name, :code, :value, :description)', $setting);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
