<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210313195404 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'twitch-streamer.overview', 'hash' => '96bbab699c6140c6eed5a55258566db9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Twitch streamers', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.twitch-streamer.title', 'hash' => '9222ef4aa09d45d02db59e904b0d3177', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Twitch streamers', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.twitch-streamer.description', 'hash' => '558755fd43700a6d777992cb3b9a00c9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat twitch streamery', 'plural1' => '', 'plural2' => ''],
            ['original' => 'twitch-streamer.overview.title', 'hash' => 'e9491a87e21f3044a7cbd82e7c1db593', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Twitch streamers|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.twitch-streamer.overview.is-active', 'hash' => 'ca4218ab674e6ed49e6c21c5f5d004fb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.twitch-streamer.overview.action.new', 'hash' => 'cc8d57bc2eb4f38d6f45ba34137b4ed7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit streamera', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.twitch-streamer.overview.name', 'hash' => 'e471097b06ba4a8dfad371c932668cb1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.twitch-streamer.overview.website', 'hash' => 'd4d293c0034e67798c3bba579f423b78', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'twitch-streamer.edit.title', 'hash' => '0123d9a8dcd51f17ef73c945123614d5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení streamera', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.name', 'hash' => 'bd917a6dac1b76c135574a61eda60486', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.name.req', 'hash' => '0a21979971237b4f953acfb6ccdbc55b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.code', 'hash' => '01a1a7bed046ee500aaf45c2a3425203', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Twitch kód', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.website', 'hash' => '29aad676997a254526c0e2def1e71171', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.image-preview', 'hash' => '8b0e83559758a1c22a2c0a536b818da1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled streamera', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.image-preview.rule-image', 'hash' => '639ea3cf0305a3aa4fb2efb314a31885', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.is-active', 'hash' => '6763b2da37cf38abdf52996eac6f00df', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.send', 'hash' => '6d1cf6d02b627c6036f225bb29daf9a8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.send-back', 'hash' => '17b1c870e6e939e572d68ed61687481b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.back', 'hash' => 'd0e7fe48f6977b17cf608d0a69c27f9b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'twitch-streamer.edit.title - %s', 'hash' => '66a6f1aa7bcd0f4a838a387d875d6877', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace streamera', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.flash.success.create', 'hash' => '96b94c2c48456be9befbc646bb0f5022', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Streamer byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.twitch-streamer.edit.flash.success.update', 'hash' => '4262df6d122a39e30a095c77a991d2a8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Streamer byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.twitch-streamer.overview.action.edit', 'hash' => 'd942327ca5b2a2b78dfbce5e0619983d', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.twitch-streamer.overview.action.flash.sort.success', 'hash' => 'd8c48d6c825eb66d0bface993642a073', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí streamerů bylo upraveno.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
